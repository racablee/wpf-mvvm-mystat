﻿using ContactListApp.Model;
using ContactListApp.Services;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ContactListApp.ViewModel
{
    class HomeworkViewModel : ViewModelBase
    {

        private ObservableCollection<Homework> overdueHomeList = new ObservableCollection<Homework>();
        public ObservableCollection<Homework> OverdueHomeList { get => overdueHomeList; set => Set(ref overdueHomeList, value); }

        private ObservableCollection<Homework> underReviewHomeList = new ObservableCollection<Homework>();
        public ObservableCollection<Homework> UnderReviewHomeList { get => underReviewHomeList; set => Set(ref underReviewHomeList, value); }

        private ObservableCollection<Homework> reviewedHomeList = new ObservableCollection<Homework>();
        public ObservableCollection<Homework> ReviewedHomeList { get => reviewedHomeList; set => Set(ref reviewedHomeList, value); }

        private ObservableCollection<Homework> currentHomeList = new ObservableCollection<Homework>();
        public ObservableCollection<Homework> CurrentHomeList { get => currentHomeList; set => Set(ref currentHomeList, value); }



        public string Token { get; set; }
        public User User { get; set; }
        public Homework Homework { get; set; }



        public HomeworkViewModel()
        {

            Messenger.Default.Register<User>(this, param =>
            {
                User = param as User;
            });    
                Token =  GetToken();
                GetTasks();
                GetOnReviewhometask();
                GetReviewedHomeTask();
            GetCurrenthometask();
        }

        public string GetToken()
        {

            string res = "";
            try
            {


                var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://msapi.itstep.org/api/v1/auth/login");
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                httpWebRequest.Headers.Add("Authorization", "Bearer null");
                httpWebRequest.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = "{\"application_key\":\"6a56a5df2667e65aab73ce76d1dd737f7d1faef9c52e8b8c55ac75f565d8e8a6\",\"id_city\":42,\"password\":\"" + "Idiotsyudatvar" + "\",\"username\":\"" + "Mame_bf86" + "\"}";
                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    dynamic obj = JsonConvert.DeserializeObject(result);
                    res = obj["access_token"].ToString();
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("422"))
                {
                    MessageBox.Show("Username or password was incorrect!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    res = "";
                }
            }
            return res;
        }

        private async void GetTasks()
        {
            await GetHomeTask();
        }


        private Task GetCurrenthometask()
        {
            return Task.Run(() =>
            {
                try
                {
                    var request = (HttpWebRequest)WebRequest.Create("https://msapi.itstep.org/api/v1/homework/operations/list?page=1&status=3&type=0&group_id=2508");
                    request.Method = "GET";
                    request.Headers.Add("Authorization", "Bearer " + Token);
                    request.ContentType = "application/x-www-form-urlencoded";
                    request.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36";
                    var response = (HttpWebResponse)request.GetResponse();
                    string res = new StreamReader(response.GetResponseStream()).ReadToEnd();
                    dynamic OverdueHomeworks = JsonConvert.DeserializeObject(res);

                    for (int i = 0; i < OverdueHomeworks.Count; i++)
                    {
                        App.Current.Dispatcher.Invoke(() =>
                        {

                            CurrentHomeList.Add(new Homework(OverdueHomeworks[i].name_spec.ToString(),
                                OverdueHomeworks[i].comment.ToString(),
                                OverdueHomeworks[i].completion_time.ToString(),
                                OverdueHomeworks[i].creation_time.ToString(),
                                OverdueHomeworks[i].file_path.ToString(),
                                OverdueHomeworks[i].filename.ToString(),
                                OverdueHomeworks[i].fio_teach.ToString(),
                                OverdueHomeworks[i].overdue_time.ToString(),
                                OverdueHomeworks[i].status.ToString(),
                                OverdueHomeworks[i].theme.ToString(),
                                 null));
                        });
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            });
        }

        private RelayCommand downloadFile;
        public RelayCommand DownloadFile
        {
            get => downloadFile ?? (downloadFile = new RelayCommand(
                () =>
                {
                    
                }
            ));
        }
        private Task GetHomeTask()
        {
            return Task.Run(() =>
            {
                try
                {
                    var request = (HttpWebRequest)WebRequest.Create("https://msapi.itstep.org/api/v1/homework/operations/list?page=1&status=0&type=0&group_id=2508");
                    request.Method = "GET";
                    request.Headers.Add("Authorization", "Bearer " + Token);
                    request.ContentType = "application/x-www-form-urlencoded";
                    request.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36";
                    var response = (HttpWebResponse)request.GetResponse();
                    string res = new StreamReader(response.GetResponseStream()).ReadToEnd();
                    dynamic OverdueHomeworks = JsonConvert.DeserializeObject(res);


                    for (int i = 0; i < OverdueHomeworks.Count; i++)
                    {
                        App.Current.Dispatcher.Invoke(() =>
                        {

                            OverdueHomeList.Add(new Homework(OverdueHomeworks[i].name_spec.ToString(), OverdueHomeworks[i].comment.ToString(), OverdueHomeworks[i].completion_time.ToString(), OverdueHomeworks[i].creation_time.ToString(), OverdueHomeworks[i].file_path.ToString(), OverdueHomeworks[i].filename.ToString(), OverdueHomeworks[i].fio_teach.ToString(), OverdueHomeworks[i].overdue_time.ToString(), OverdueHomeworks[i].status.ToString(), OverdueHomeworks[i].theme.ToString(),null));
                        });

                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            });
        }

        private Task GetOnReviewhometask()
        {
            return Task.Run(() =>
            {
                try
                {
                    var request = (HttpWebRequest)WebRequest.Create("https://msapi.itstep.org/api/v1/homework/operations/list?page=1&status=2&type=0&group_id=2508");
                    request.Method = "GET";
                    request.Headers.Add("Authorization", "Bearer " + Token);
                    request.ContentType = "application/x-www-form-urlencoded";
                    request.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36";
                    var response = (HttpWebResponse)request.GetResponse();
                    string res = new StreamReader(response.GetResponseStream()).ReadToEnd();
                    dynamic OverdueHomeworks = JsonConvert.DeserializeObject(res);

                    for (int i = 0; i < OverdueHomeworks.Count; i++)
                    {
                        App.Current.Dispatcher.Invoke(() =>
                        {

                            UnderReviewHomeList.Add(new Homework(OverdueHomeworks[i].name_spec.ToString(),
                                OverdueHomeworks[i].comment.ToString(),
                                OverdueHomeworks[i].completion_time.ToString(),
                                OverdueHomeworks[i].creation_time.ToString(),
                                OverdueHomeworks[i].file_path.ToString(),
                                OverdueHomeworks[i].filename.ToString(),
                                OverdueHomeworks[i].fio_teach.ToString(),
                                OverdueHomeworks[i].overdue_time.ToString(),
                                OverdueHomeworks[i].status.ToString(),
                                OverdueHomeworks[i].theme.ToString(),
                                 null));
                        });
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            });
        }

        private Task GetReviewedHomeTask()
        {
            return Task.Run(() =>
            {
                try
                {
                    var request = (HttpWebRequest)WebRequest.Create("https://msapi.itstep.org/api/v1/homework/operations/list?page=1&status=1&type=0&group_id=2508");
                    request.Method = "GET";
                    request.Headers.Add("Authorization", "Bearer " + Token);
                    request.ContentType = "application/x-www-form-urlencoded";
                    request.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36";
                    var response = (HttpWebResponse)request.GetResponse();
                    string res = new StreamReader(response.GetResponseStream()).ReadToEnd();
                    dynamic OverdueHomeworks = JsonConvert.DeserializeObject(res);


                    for (int i = 0; i < OverdueHomeworks.Count; i++)
                    {
                        App.Current.Dispatcher.Invoke(() =>
                        {

                            ReviewedHomeList.Add(new Homework(OverdueHomeworks[i].name_spec.ToString(), OverdueHomeworks[i].comment.ToString(), OverdueHomeworks[i].completion_time.ToString(), OverdueHomeworks[i].creation_time.ToString(), OverdueHomeworks[i].file_path.ToString(), OverdueHomeworks[i].filename.ToString(), OverdueHomeworks[i].fio_teach.ToString(), OverdueHomeworks[i].overdue_time.ToString(), OverdueHomeworks[i].status.ToString(), OverdueHomeworks[i].theme.ToString(),null));
                        });

                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            });
        }

    }
}
