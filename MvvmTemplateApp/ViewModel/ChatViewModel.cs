﻿using ContactListApp.Model;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace ContactListApp.ViewModel
{
    public class ScrollingListBox : ListBox
    {
        protected override void OnItemsChanged(System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            try
            {

            int newItemCount = e.NewItems.Count;

            if (newItemCount > 0)
                this.ScrollIntoView(e.NewItems[newItemCount - 1]);

            base.OnItemsChanged(e);
            }
            catch (Exception)
            {

            }
        }
    }
	

    class ChatViewModel : ViewModelBase
    {
        private User user;
        public User User { get => user; set => Set(ref user, value); }

        private string message;
        public string Message { get => message; set => Set(ref message, value); }

        private ObservableCollection<Message> messagesList;
        public ObservableCollection<Message> MessagesList { get => messagesList; set => Set(ref messagesList, value); }

        public ChatViewModel()
        {
            MessagesList = new ObservableCollection<Message>();
            Messenger.Default.Register<User>(this, param =>
            {
                User = param as User;

            });


            //MessagesList.Add(new Message("https://mystatfiles.itstep.org/index.php?view_key=rtILv2awXkYrSQ7WVzOr0G9F1kZwIdRQC03dLrvYiKdt6oo34%2B44sm6AsTVkW5hk6FTkGa9REuL7iVY7IAN3C9Bx8eOt11OoxkYbhcQxT2o%3D", "Раджабли Искендер Гусейн", "Hello!", new DateTime()));
            System.Windows.Threading.DispatcherTimer timer = new System.Windows.Threading.DispatcherTimer();
            timer.Tick += new EventHandler(timer_Tick);
            timer.Interval = new TimeSpan(0, 0, 0, 0, 100);
            timer.Start();
        }

        private async void timer_Tick(object sender, EventArgs e)
        {
            await Start();
        }






        private Task Start()
        {
            return Task.Run(() =>
            {

                string conStr = "Data Source=den1.mssql8.gear.host;Initial Catalog=mystat;User ID=mystat;Password=Zn8DeRt-~UMp";
                using (SqlConnection connection = new SqlConnection(conStr))
                {
                    connection.Open();
                    //
                    // SqlCommand should be created inside using.
                    // ... It receives the SQL statement.
                    // ... It receives the connection object.
                    // ... The SQL text works with a specific database.
                    //
                    using (SqlCommand command = new SqlCommand(
                        "select * from Messages Order by[Time] Asc",
                        connection))
                    {
                        //
                        // Instance methods can be used on the SqlCommand.
                        // ... These read data.
                        //
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                bool can = true;
                                for (int i = 0; i < reader.FieldCount; i++)
                                {
                                    if (i == 0)
                                    {
                                        for (int j = 0; j < MessagesList.Count; j++)
                                        {
                                            if (reader.GetInt32(i) == MessagesList[j].Id)
                                            {
                                                can = false;
                                            }

                                        }
                                    }
                                    else if(reader.GetString(1) != User.Group)
                                    {
                                        can = false;
                                    }
                                }

                                if (can)
                                {
                                    App.Current.Dispatcher.Invoke( () =>
                                    {
                                       MessagesList.Add(new Message(reader.GetInt32(0), reader.GetString(2), reader.GetString(3),reader.GetString(4),reader.GetValue(5).ToString()));



                                    });
                                    

                                }

                            }
                        }

                    }
                }


            });
        }



        private RelayCommand sendMessageCommand;
        public RelayCommand SendMessageCommand
        {
            get => sendMessageCommand ?? (sendMessageCommand = new RelayCommand(
                async () =>
                {
                   // "insert into Messages values('FSDA_2713_ru','https://mystatfiles.itstep.org/index.php?view_key=rtILv2awXkYrSQ7WVzOr0G9F1kZwIdRQC03dLrvYiKdt6oo34%2B44sm6AsTVkW5hk6FTkGa9REuL7iVY7IAN3C9Bx8eOt11OoxkYbhcQxT2o%3D',N'Раджабли Искендер Гусейн',N'Helloщщщщщщщщ',GETDATE())";
                    await SendMessage(Message);
                    Message = "";
                }
            ));
        }


        private Task SendMessage(string message)
        {
            return Task.Run(() =>
            {
            string conStr = "Data Source=den1.mssql8.gear.host;Initial Catalog=mystat;User ID=mystat;Password=Zn8DeRt-~UMp";
                SqlConnection connection = new SqlConnection(conStr);
            
                connection.Open();
                    SqlCommand command = new SqlCommand(
                        $"insert into Messages values('{User.Group}', '{User.PictureUrl}', N'{User.Fullname}', N'{Message}', GETDATE())",
                        connection);
                command.ExecuteNonQuery();
            });
        }
    }
}
