﻿using ContactListApp.Model;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ContactListApp.ViewModel
{
    class MainPageViewModel : ViewModelBase
    {
        public string Token { get; set; }

        private ObservableCollection<User> streamLeadersList = new ObservableCollection<User>();
        public ObservableCollection<User> StreamLeadersList { get => streamLeadersList; set => Set(ref streamLeadersList, value); }

        private string allTasks;
        public string AllTasks { get => allTasks; set => Set(ref allTasks, value); }

        private string current;
        public string Current { get => current; set => Set(ref current, value); }

        private string reviewed;
        public string Reviewed { get => reviewed; set => Set(ref reviewed, value); }

        private string underReview;
        public string UnderReview { get => underReview; set => Set(ref underReview, value); }


        private string overdue;
        public string Overdue { get => overdue; set => Set(ref overdue, value); }




        public MainPageViewModel()
        {
            Messenger.Default.Register<string>(this, param =>
            {
                Token = param as string;

            });

            GetLeaders();
            GetHomeworkInfo();
        }


        private async void GetLeaders()
        {
            await GetLeadersTask();
        }

        private async void GetHomeworkInfo()
        {
            await GetHomeworkInfoTask();
        }

        private Task GetHomeworkInfoTask()
        {
            return Task.Run(() =>
            {
                try
                {
                    var request = (HttpWebRequest)WebRequest.Create("https://msapi.itstep.org/api/v1/count/homework");
                    request.Method = "GET";
                    request.Headers.Add("Authorization", "Bearer " + Token);
                    request.ContentType = "application/x-www-form-urlencoded";
                    request.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36";
                    var response = (HttpWebResponse)request.GetResponse();
                    string res = new StreamReader(response.GetResponseStream()).ReadToEnd();
                    dynamic home = JsonConvert.DeserializeObject(res);
                        App.Current.Dispatcher.Invoke(() =>
                        {

                            AllTasks = home[5].counter.ToString();

                            Current = home[1].counter.ToString();
                            Reviewed = home[0].counter.ToString();
                            Overdue = home[2].counter.ToString();
                            UnderReview = home[3].counter.ToString();
                        });

                    
                }
                catch (Exception ex)
                {

                }
            });
        }


        private Task GetLeadersTask()
        {
            return Task.Run(() =>
            {
                try
                {
                    var request = (HttpWebRequest)WebRequest.Create("https://msapi.itstep.org/api/v1/dashboard/progress/leader-group");
                    request.Method = "GET";
                    request.Headers.Add("Authorization", "Bearer " + Token);
                    request.ContentType = "application/x-www-form-urlencoded";
                    request.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36";
                    var response = (HttpWebResponse)request.GetResponse();
                    string res = new StreamReader(response.GetResponseStream()).ReadToEnd();
                    dynamic leaders = JsonConvert.DeserializeObject(res);

                    
                    for (int i = 0; i < leaders.Count; i++)
                    {
                        App.Current.Dispatcher.Invoke(() =>
                        {

                            StreamLeadersList.Add(new User(null,null,null,null,i+1 + ". " + leaders[i].full_name.ToString(),null, leaders[i].amount.ToString(),null,null,null,Token));
                        });
                        
                    }
                }
                catch (Exception ex) {
                    
                }
            });
        }

        

        //private RelayCommand goBackCommand;
        //public RelayCommand GoBackCommand
        //{
        //    get => goBackCommand ?? (goBackCommand = new RelayCommand(
        //        () =>
        //        {

        //            NavigationService.Navigate("Main");
        //        }
        //    ));
        //}



    }
}
