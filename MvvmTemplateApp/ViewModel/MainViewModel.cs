﻿using ContactListApp.Model;
using ContactListApp.Services;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace ContactListApp.ViewModel
{
    class MainViewModel : ViewModelBase
    {
        private string boxUsername;
        public string BoxUsername { get => boxUsername; set => Set(ref boxUsername, value); } 

        private string boxPassword;
        public string BoxPassword { get => boxPassword; set => Set(ref boxPassword, value); } 


        public INavigationService NavigationService { get; }



     
        public MainViewModel(INavigationService navigationService, ITestService testService)
        {
            NavigationService = navigationService;
        

        }

        private RelayCommand authCommand;
        public RelayCommand AuthCommand
        {
            get => authCommand ?? (authCommand = new RelayCommand(
                async () => 
                {
                    string token = await GetToken();
                    if(!String.IsNullOrEmpty(token))
                    {
                        // GET USER INFO JSON
                        string userInfoJson = await GetUserInfo(token);
                        dynamic userInfo = JsonConvert.DeserializeObject(userInfoJson);
                        int pointsSumInt = Int32.Parse(userInfo.gaming_points[0].points.ToString()) + Int32.Parse(userInfo.gaming_points[1].points.ToString());
                        User user = new User(userInfo["photo"].ToString(),userInfo["level"].ToString(), BoxUsername,BoxPassword,userInfo["full_name"].ToString(),userInfo["group_name"].ToString(), pointsSumInt.ToString(), userInfo.gaming_points[0].points.ToString(), userInfo.gaming_points[1].points.ToString(),userInfo.achieves_count.ToString(),token.ToString());

                        Messenger.Default.Send(user);
                        Messenger.Default.Send(token);
                        NavigationService.Navigate("Second");

                    }
                }
            ));
        }


        public Task<string> GetUserInfo(string token)
        {
            return Task.Run(() =>
            {
                string res = "";
                try
                {
                    var request = (HttpWebRequest)WebRequest.Create("https://msapi.itstep.org/api/v1/settings/user-info");

                    request.Method = "GET";
                    request.Headers.Add("Authorization", "Bearer " + token);
                    request.ContentType = "application/x-www-form-urlencoded";
                    request.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36";
                    var response = (HttpWebResponse)request.GetResponse();
                    res = new StreamReader(response.GetResponseStream()).ReadToEnd();
                }
                catch (Exception ex)
                {
                    res = "";
                }
                return res;
            });
        }
        
        public Task<string> GetToken()
        {
            return Task.Run(() =>
            {
                string res = "";
                try
                {


                    var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://msapi.itstep.org/api/v1/auth/login");
                    httpWebRequest.ContentType = "application/json";
                    httpWebRequest.Method = "POST";
                    httpWebRequest.Headers.Add("Authorization", "Bearer null");
                    httpWebRequest.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36";

                    using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                    {
                        string json = "{\"application_key\":\"6a56a5df2667e65aab73ce76d1dd737f7d1faef9c52e8b8c55ac75f565d8e8a6\",\"id_city\":42,\"password\":\"" + BoxPassword + "\",\"username\":\"" + BoxUsername + "\"}";
                        streamWriter.Write(json);
                        streamWriter.Flush();
                        streamWriter.Close();
                    }

                    var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                    using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        var result = streamReader.ReadToEnd();
                        dynamic obj = JsonConvert.DeserializeObject(result);
                        res = obj["access_token"].ToString();
                    }
                }
                catch (Exception ex)
                {
                    if(ex.Message.Contains("422"))
                    {
                        MessageBox.Show("Username or password was incorrect!","Error",MessageBoxButton.OK,MessageBoxImage.Error);
                        res = "";
                    }
                }
                return res;
            });
        }
    }
}
