﻿using Autofac;
using ContactListApp.Model;
using ContactListApp.Services;
using ContactListApp.View;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ContactListApp.ViewModel
{
    class SecondViewModel : ViewModelBase
    {
        public INavigationService NavigationService { get; }

        private User user;
        public User User { get => user; set => Set(ref user, value); }

        private ViewModelBase currentPage;
        public ViewModelBase CurrentPage { get => currentPage; set => Set(ref currentPage, value); }

        public string Token { get; set; }

        public SecondViewModel(INavigationService navigationService)
        {
            NavigationService = navigationService;

            Messenger.Default.Register<User>(this, param =>
            {
                 User = param as User;
            });

            Messenger.Default.Register<string>(this, param =>
            {
                Token = param as string;

            });
        }

        private RelayCommand goBackCommand;
        public RelayCommand GoBackCommand
        {
            get => goBackCommand ?? (goBackCommand = new RelayCommand(
                () =>
                {
                    
                   NavigationService.Navigate("Main");
                }
            ));
        }


        private RelayCommand chatViewCommand;
        public RelayCommand ChatViewCommand
        {
            get => chatViewCommand ?? (chatViewCommand = new RelayCommand(
                () =>
                {
                    var builder = new ContainerBuilder();
                    builder.RegisterType<ChatViewModel>();

                    var container = builder.Build();
                    using (var scope = container.BeginLifetimeScope())
                    {
                        CurrentPage = scope.Resolve<ChatViewModel>();
                        Messenger.Default.Send(User);
                    };
                    // NavigationService.Navigate("Main");
                }
            ));
        }

        private RelayCommand homeWorkViewCommand;
        public RelayCommand HomeWorkViewCommand
        {
            get => homeWorkViewCommand ?? (homeWorkViewCommand = new RelayCommand(
                () =>
                {
                    var builder = new ContainerBuilder();
                    builder.RegisterType<HomeworkViewModel>();

                    var container = builder.Build();
                    using (var scope = container.BeginLifetimeScope())
                    {
                        CurrentPage = scope.Resolve<HomeworkViewModel>();
                        Messenger.Default.Send(User);
                        Messenger.Default.Send(Token);
                    };


                    
                    // NavigationService.Navigate("Main");
                }
            ));
        }

        private RelayCommand mainPageViewCommand;
        public RelayCommand MainPageViewCommand
        {
            get => mainPageViewCommand ?? (mainPageViewCommand = new RelayCommand(
                () =>
                {
                    var builder = new ContainerBuilder();
                    builder.RegisterType<MainPageViewModel>();

                    var container = builder.Build();
                    using (var scope = container.BeginLifetimeScope())
                    {
                        CurrentPage = scope.Resolve<MainPageViewModel>();
                        Messenger.Default.Send(Token);
                    };
                    // NavigationService.Navigate("Main");
                }
            ));
        }




    }
}
