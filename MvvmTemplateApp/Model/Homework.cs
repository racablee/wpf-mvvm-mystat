﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactListApp.Model
{
    class Homework
    {
        public Homework(string spec_Name, string comment, string completion_time, string creation_time, string file_path, string filename, string fio_teach, string overdue_time, string status, string theme,string sendByStudent = null)
        {
            Spec_Name = spec_Name;
            Comment = comment;
            Completion_time = completion_time;
            Creation_time = creation_time;
            File_path = file_path;
            Filename = filename;
            Fio_teach = fio_teach;
            Overdue_time = overdue_time;
            Status = status;
            Theme = theme;
            SendByStudent = sendByStudent;
        }

        public string Spec_Name { get; set; }
        public string Comment { get; set; }
        public string Completion_time { get; set; }
        public string Creation_time { get; set; }
        public string File_path { get; set; }
        public string Filename { get; set; }
        public string Fio_teach { get; set; }
        public string Overdue_time { get; set; }
        public string Status { get; set; }
        public string Theme { get; set; }
        public string SendByStudent { get; set; }
        public string downloadpathStudent { get; set; }
    }
}
