﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactListApp.Model
{
    class Message : ObservableObject
    {
        public Message(int id,string pictureUrl, string fullname, string messageStr, string time)
        {
            Id = id;
            PictureUrl = pictureUrl;
            Fullname = fullname;
            MessageStr = messageStr;
            Time = time;
        }

        public int Id { get; set; }
        public string PictureUrl { get; set; }
        public string Fullname { get; set; }
        public string MessageStr { get; set; }
        public string Time { get; set; }
    }
}
