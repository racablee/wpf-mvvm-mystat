﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactListApp.Model
{
    class User : ObservableObject
    {
        public User(string pictureUrl, string level, string username, string password, string fullname, string group, string points, string crystals, string coins, string badges, string token)
        {
            PictureUrl = pictureUrl;
            Level = level;
            Username = username;
            Password = password;
            Fullname = fullname;
            Group = group;
            Points = points;
            Crystals = crystals;
            Coins = coins;
            Badges = badges;
            Token = token;
        }

        public string PictureUrl { get; set; }
        public string Level { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Fullname { get; set; }
        public string Group { get; set; }
        public string Points { get; set; }
        public string Crystals { get; set; }
        public string Coins { get; set; }
        public string Badges { get; set; }
        public string Token { get; set; }
    }
}
